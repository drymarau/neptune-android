package io.github.drymarev.neptune.util

import com.google.common.truth.Truth.assertThat
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import java.io.InputStream

fun mockWebServer(): MockWebServer {
  return MockWebServer()
}

inline fun mockWebServer(actions: MockWebServer.() -> Unit): MockWebServer {
  return mockWebServer().apply(actions)
}

inline fun mockResponse(actions: MockResponse.() -> Unit): MockResponse {
  return MockResponse().apply(actions)
}

fun MockResponse.setBody(stream: InputStream) {
  setBody(stream.readUtf8())
}

inline fun MockWebServer.takeRequest(actions: RecordedRequest.() -> Unit) {
  takeRequest().run(actions)
}

fun MockWebServer.assertRequestCount(count: Int) {
  assertThat(requestCount).isEqualTo(count)
}
