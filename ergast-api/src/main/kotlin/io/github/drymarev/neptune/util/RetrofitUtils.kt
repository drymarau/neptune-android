package io.github.drymarev.neptune.util

import retrofit2.Retrofit

inline fun buildRetrofit(actions: Retrofit.Builder.() -> Unit): Retrofit {
  return Retrofit.Builder().apply(actions).build()
}

inline fun <reified T> buildRetrofitService(actions: Retrofit.Builder.() -> Unit): T {
  return buildRetrofit(actions).create(T::class.java)
}
