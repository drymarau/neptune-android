package io.github.drymarev.neptune.util

import okhttp3.OkHttpClient
import okhttp3.OkHttpClient.Builder

fun buildOkHttpClient(): OkHttpClient {
  return OkHttpClient()
}

fun buildOkHttpClient(actions: Builder.() -> Unit): OkHttpClient {
  return OkHttpClient.Builder().apply(actions).build()
}

fun buildOkHttpClient(client: OkHttpClient, actions: Builder.() -> Unit): OkHttpClient {
  return client.newBuilder().apply(actions).build()
}
