package io.github.drymarev.neptune.util

import android.support.annotation.IdRes
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction

val FragmentActivity.fm: FragmentManager
  inline get() {
    return supportFragmentManager
  }

inline fun FragmentManager.commit(actions: FragmentTransaction.() -> Unit) {
  beginTransaction().apply(actions).commit()
}

operator fun FragmentManager.contains(@IdRes id: Int): Boolean {
  return findFragmentById(id) != null
}
