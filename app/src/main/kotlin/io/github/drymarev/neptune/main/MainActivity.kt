package io.github.drymarev.neptune.main

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.BottomNavigationView.OnNavigationItemSelectedListener
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import dagger.Module
import dagger.android.AndroidInjection
import dagger.android.ContributesAndroidInjector
import io.github.drymarev.neptune.PerActivity
import io.github.drymarev.neptune.R
import io.github.drymarev.neptune.constructor_standings.ConstructorStandingsFragment
import io.github.drymarev.neptune.databinding.MainActivityBinding
import io.github.drymarev.neptune.driver_standings.DriverStandingsFragment
import io.github.drymarev.neptune.schedule.ScheduleFragment
import io.github.drymarev.neptune.util.commit
import io.github.drymarev.neptune.util.consume
import io.github.drymarev.neptune.util.contains
import io.github.drymarev.neptune.util.fm

class MainActivity : AppCompatActivity(), OnNavigationItemSelectedListener {

  private lateinit var binding: MainActivityBinding

  override fun onCreate(savedInstanceState: Bundle?) {
    AndroidInjection.inject(this)
    super.onCreate(savedInstanceState)
    binding = DataBindingUtil.setContentView(this, R.layout.main_activity)
    binding.bottomNavigation.setOnNavigationItemSelectedListener(this)

    if (R.id.container in fm) {
      return
    }
    fm.commit { replace(R.id.container, ScheduleFragment.newInstance()) }
  }

  override fun onDestroy() {
    binding.bottomNavigation.setOnNavigationItemSelectedListener(null)
    super.onDestroy()
  }

  override fun onNavigationItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.action_schedule -> consume {
        fm.commit { replace(R.id.container, ScheduleFragment.newInstance()) }
      }
      R.id.action_drivers -> consume {
        fm.commit { replace(R.id.container, DriverStandingsFragment.newInstance()) }
      }
      R.id.action_constructors -> consume {
        fm.commit { replace(R.id.container, ConstructorStandingsFragment.newInstance()) }
      }
      else -> false
    }
  }
}

@Module interface MainActivityModule {

  @PerActivity @ContributesAndroidInjector fun activity(): MainActivity
}
