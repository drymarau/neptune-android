package io.github.drymarev.neptune

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.ViewModelProvider
import android.support.v4.app.Fragment
import com.squareup.moshi.Moshi
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.HasSupportFragmentInjector
import hu.supercluster.paperwork.Paperwork
import io.github.drymarev.neptune.constructor_standings.ConstructorStandingsFragmentModule
import io.github.drymarev.neptune.constructor_standings.ConstructorStandingsPresenterModule
import io.github.drymarev.neptune.constructor_standings.ConstructorStandingsViewModel
import io.github.drymarev.neptune.driver_standings.DriverStandingsFragmentModule
import io.github.drymarev.neptune.driver_standings.DriverStandingsPresenterModule
import io.github.drymarev.neptune.driver_standings.DriverStandingsViewModel
import io.github.drymarev.neptune.main.MainActivityModule
import io.github.drymarev.neptune.schedule.ScheduleFragmentModule
import io.github.drymarev.neptune.schedule.SchedulePresenterModule
import io.github.drymarev.neptune.schedule.ScheduleViewModel
import io.github.drymarev.neptune.util.buildMoshi
import io.github.drymarev.neptune.util.buildOkHttpClient
import io.github.drymarev.neptune.util.buildRetrofit
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class App : Application(), HasActivityInjector, HasSupportFragmentInjector {

  @Inject lateinit var activityInjector: DispatchingAndroidInjector<Activity>
  @Inject lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>
  @Inject lateinit var tree: Timber.Tree

  private val app: Application
    inline get() {
      return this
    }

  override fun onCreate() {
    super.onCreate()
    injectComponent {
      app(app)
      debug(BuildConfig.DEBUG)
      baseUrl(getString(R.string.base_url))
    }
    Timber.plant(tree)
  }

  override fun activityInjector(): AndroidInjector<Activity> = activityInjector

  override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

  private inline fun injectComponent(actions: AppComponent.Builder.() -> Unit) {
    DaggerAppComponent.builder().apply(actions).build().inject(this)
  }
}

@Component(modules = arrayOf(
    AndroidSupportInjectionModule::class,
    PaperworkModule::class,
    TimberTreeModule::class,
    MainThreadSchedulerModule::class,
    ErgastServiceModule::class,
    MainActivityModule::class,
    ScheduleFragmentModule::class,
    DriverStandingsFragmentModule::class,
    ConstructorStandingsFragmentModule::class,
    ViewModelModule::class
))
@PerApp interface AppComponent {

  fun inject(app: App)

  @Component.Builder interface Builder {

    @BindsInstance fun app(app: Application): Builder

    @BindsInstance fun debug(@Named("debug") debug: Boolean): Builder

    @BindsInstance fun baseUrl(@Named("base_url") url: String): Builder

    fun build(): AppComponent
  }
}

@Subcomponent interface ViewModelComponent {

  fun scheduleViewModel(): ScheduleViewModel

  fun driverStandingsViewModel(): DriverStandingsViewModel

  fun constructorStandingsViewModel(): ConstructorStandingsViewModel

  @Subcomponent.Builder interface Builder {
    fun build(): ViewModelComponent
  }
}

@Module(
    subcomponents = arrayOf(ViewModelComponent::class),
    includes = arrayOf(
        SchedulePresenterModule::class,
        DriverStandingsPresenterModule::class,
        ConstructorStandingsPresenterModule::class
    )
) class ViewModelModule {

  @Provides
  @PerApp fun provide(builder: ViewModelComponent.Builder): ViewModelProvider.Factory {
    return AppViewModelFactory(builder.build())
  }
}

@Module class PaperworkModule {

  @Provides
  @PerApp fun provide(application: Application): Paperwork {
    return Paperwork(application)
  }
}

@Module class TimberTreeModule {

  @Provides
  @PerApp fun provide(@Named("debug") debug: Boolean): Timber.Tree {
    return if (debug) Timber.DebugTree() else CrashReportingTree()
  }

  private class CrashReportingTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String?, t: Throwable?) {
      TODO("Add crash reporting.")
    }
  }
}

@Module class HttpLoggingInterceptorModule {

  @Provides
  @PerApp fun provide(@Named("debug") debug: Boolean): HttpLoggingInterceptor {
    val interceptor = HttpLoggingInterceptor { Timber.tag("OkHttp").d(it) }
    interceptor.level = if (debug) Level.BODY else Level.BASIC
    return interceptor
  }
}

@Module(includes = arrayOf(HttpLoggingInterceptorModule::class)) class OkHttpClientModule {

  @Provides
  @PerApp fun provide(interceptor: HttpLoggingInterceptor): OkHttpClient {
    return buildOkHttpClient { addInterceptor(interceptor) }
  }
}

@Module class MoshiModule {

  @Provides
  @PerApp fun provide(): Moshi {
    return buildMoshi {
      add(RaceJsonAdapter())
      add(LocalDateAdapter())
    }
  }
}

@Module(includes = arrayOf(MoshiModule::class)) class MoshiConverterFactoryModule {

  @Provides
  @Named("json")
  @PerApp fun provide(moshi: Moshi): Converter.Factory {
    return MoshiConverterFactory.create(moshi)
  }
}

@Module class MRDataConverterFactoryModule {

  @Provides
  @Named("mr_data")
  @PerApp fun provide(): Converter.Factory {
    return MRDataConverterFactory.create()
  }
}

@Module class RxJava2CallAdapterFactoryModule {

  @Provides
  @Named("rxjava2")
  @PerApp fun provide(): CallAdapter.Factory {
    return RxJava2CallAdapterFactory.createAsync()
  }
}

@Module(includes = arrayOf(
    OkHttpClientModule::class,
    MRDataConverterFactoryModule::class,
    MoshiConverterFactoryModule::class,
    RxJava2CallAdapterFactoryModule::class
)) class RetrofitModule {

  @Provides
  @PerApp fun provide(
      @Named("base_url") url: String,
      client: OkHttpClient,
      @Named("mr_data") mrDataConverterFactory: Converter.Factory,
      @Named("json") jsonConverterFactory: Converter.Factory,
      @Named("rxjava2") rxJava2CallAdapterFactory: CallAdapter.Factory
  ): Retrofit {
    return buildRetrofit {
      baseUrl(url)
      client(client)
      addConverterFactory(mrDataConverterFactory)
      addConverterFactory(jsonConverterFactory)
      addCallAdapterFactory(rxJava2CallAdapterFactory)
    }
  }
}

@Module(includes = arrayOf(RetrofitModule::class)) class ErgastServiceModule {

  @Provides
  @PerApp fun provide(retrofit: Retrofit): ErgastService {
    return retrofit.create(ErgastService::class.java)
  }
}

@Module class MainThreadSchedulerModule {

  @Provides
  @Named("main")
  @PerApp fun provide(): Scheduler {
    return AndroidSchedulers.mainThread()
  }
}
