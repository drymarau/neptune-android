package io.github.drymarev.neptune.schedule

import android.arch.lifecycle.ViewModel
import com.github.ajalt.timberkt.i
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.github.drymarev.neptune.core.Presenter
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import javax.inject.Inject

class ScheduleViewModel @Inject constructor(
    val presenter: Presenter<@JvmSuppressWildcards ScheduleView>
) : ViewModel(), ScheduleView {

  private val loadScheduleIntentRelay = PublishRelay.create<LoadScheduleIntent>()
  private val modelRelay = BehaviorRelay.create<ScheduleModel>()

  val loadScheduleIntentConsumer: Consumer<LoadScheduleIntent>
    get() {
      return loadScheduleIntentRelay
    }
  val models: Observable<ScheduleModel>
    get() {
      return modelRelay.hide()
    }

  init {
    presenter.attach(this)
  }

  override fun onCleared() {
    presenter.detach(this)
  }

  override fun loadScheduleIntent(): Observable<LoadScheduleIntent> {
    return loadScheduleIntentRelay.hide().distinctUntilChanged()
  }

  override fun render(model: ScheduleModel) {
    i { model.toString() }
    modelRelay.accept(model)
  }
}
