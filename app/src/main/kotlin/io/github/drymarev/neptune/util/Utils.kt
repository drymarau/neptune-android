package io.github.drymarev.neptune.util

inline fun consume(actions: () -> Unit): Boolean {
  actions()
  return true
}
