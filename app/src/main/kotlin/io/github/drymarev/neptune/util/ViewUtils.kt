package io.github.drymarev.neptune.util

import android.view.LayoutInflater
import android.view.View

val View.inflater: LayoutInflater
  inline get() {
    return LayoutInflater.from(context)
  }
