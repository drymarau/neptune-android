package io.github.drymarev.neptune

import javax.inject.Scope
import kotlin.annotation.AnnotationRetention.RUNTIME

@Scope @MustBeDocumented @Retention(RUNTIME) annotation class PerApp

@Scope @MustBeDocumented @Retention(RUNTIME) annotation class PerActivity

@Scope @MustBeDocumented @Retention(RUNTIME) annotation class PerFragment
