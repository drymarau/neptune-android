package io.github.drymarev.neptune.schedule

import dagger.Binds
import dagger.Module
import dagger.Provides
import io.github.drymarev.neptune.core.Presenter
import io.github.drymarev.neptune.core.Result
import io.github.drymarev.neptune.core.UseCase

@Module(includes = arrayOf(RemoteLoadScheduleUseCaseModule::class))
interface SchedulePresenterModule {

  @Binds fun bind(presenter: SchedulePresenter): Presenter<@JvmSuppressWildcards ScheduleView>
}

@Module class RemoteLoadScheduleUseCaseModule {

  @Provides
  fun provide(useCase: RemoteLoadScheduleUseCase): UseCase<LoadScheduleAction, Result<ScheduleRecords>> {
    return useCase
  }
}
