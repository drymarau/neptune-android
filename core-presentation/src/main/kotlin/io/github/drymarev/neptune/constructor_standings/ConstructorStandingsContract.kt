package io.github.drymarev.neptune.constructor_standings

import io.github.drymarev.neptune.core.Reducer
import io.github.drymarev.neptune.core.Result
import io.github.drymarev.neptune.core.RxPresenter
import io.github.drymarev.neptune.core.UseCase
import io.github.drymarev.neptune.core.View
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

interface ConstructorStandingsView : View {

  fun loadConstructorStandingsIntent(): Observable<LoadConstructorStandingsIntent>

  fun render(model: ConstructorStandingsModel)
}

sealed class LoadConstructorStandingsIntent {
  object CurrentSeason : LoadConstructorStandingsIntent()
  data class Season(val season: String) : LoadConstructorStandingsIntent()
}

data class ConstructorItem(
    val id: String,
    val url: String,
    val name: String,
    val nationality: String
)

data class ConstructorStandingItem(
    val season: String,
    val round: String,
    val position: String,
    val positionText: String,
    val points: String,
    val wins: String,
    val constructorItem: ConstructorItem
)

data class ConstructorStandingsModel(
    val loadingConstructorStandings: Boolean = false,
    val constructorStandings: List<ConstructorStandingItem> = emptyList(),
    val constructorStandingsError: Throwable? = null
)

sealed class ConstructorStandingsPartialModel {
  object LoadingConstructorStandings : ConstructorStandingsPartialModel()
  data class ConstructorStandingsLoaded(
      val items: List<ConstructorStandingItem>
  ) : ConstructorStandingsPartialModel()

  data class ConstructorStandingsError(val error: Throwable) : ConstructorStandingsPartialModel()
}

internal object ConstructorStandingsReducer : Reducer<ConstructorStandingsModel, ConstructorStandingsPartialModel> {
  override fun invoke(model: ConstructorStandingsModel,
      changes: ConstructorStandingsPartialModel): ConstructorStandingsModel {
    return when (changes) {
      ConstructorStandingsPartialModel.LoadingConstructorStandings -> model.copy(
          loadingConstructorStandings = true,
          constructorStandings = emptyList(),
          constructorStandingsError = null
      )
      is ConstructorStandingsPartialModel.ConstructorStandingsLoaded -> model.copy(
          loadingConstructorStandings = false,
          constructorStandings = changes.items,
          constructorStandingsError = null
      )
      is ConstructorStandingsPartialModel.ConstructorStandingsError -> model.copy(
          loadingConstructorStandings = false,
          constructorStandings = emptyList(),
          constructorStandingsError = changes.error
      )
    }
  }
}

class ConstructorStandingsPresenter @Inject constructor(
    val loadConstructorStandingsUseCase: UseCase<LoadConstructorStandingsAction, Result<ConstructorStandingRecords>>
) : RxPresenter<ConstructorStandingsView>() {

  override fun attach(view: ConstructorStandingsView) {
    val loadConstructorStandings = view.loadConstructorStandingsIntent()
        .observeOn(Schedulers.computation())
        .map {
          when (it) {
            LoadConstructorStandingsIntent.CurrentSeason -> LoadConstructorStandingsAction.CurrentSeason
            is LoadConstructorStandingsIntent.Season -> LoadConstructorStandingsAction.Season(
                it.season)
          }
        }
        .compose(loadConstructorStandingsUseCase)
        .map {
          when (it) {
            is Result.Success -> ConstructorStandingsPartialModel
                .ConstructorStandingsLoaded(it.value.map { it.constructorStandingItem })
            is Result.Failure -> ConstructorStandingsPartialModel
                .ConstructorStandingsError(it.error)
          }
        }
        .startWith(ConstructorStandingsPartialModel.LoadingConstructorStandings)
    add(loadConstructorStandings.observeOn(Schedulers.computation())
        .scan(ConstructorStandingsModel(), ConstructorStandingsReducer)
        .distinctUntilChanged()
        .subscribeBy(onNext = { view.render(it) }))
  }
}

private val ConstructorStandingRecord.constructorStandingItem: ConstructorStandingItem
  inline get() {
    return ConstructorStandingItem(
        season = season,
        round = round,
        position = position,
        positionText = positionText,
        points = points,
        wins = wins,
        constructorItem = constructorRecord.constructorItem
    )
  }

internal val ConstructorRecord.constructorItem: ConstructorItem
  inline get() {
    return ConstructorItem(
        id = id,
        url = url,
        name = name,
        nationality = nationality
    )
  }
