package io.github.drymarev.neptune.schedule

import io.github.drymarev.neptune.core.Presenter
import io.github.drymarev.neptune.core.Reducer
import io.github.drymarev.neptune.core.Result
import io.github.drymarev.neptune.core.UseCase
import io.github.drymarev.neptune.core.View
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.OffsetDateTime
import javax.inject.Inject

interface ScheduleView : View {

  fun loadScheduleIntent(): Observable<LoadScheduleIntent>

  fun render(model: ScheduleModel)
}

sealed class LoadScheduleIntent {
  object CurrentSeason : LoadScheduleIntent()
  data class Season(val season: String) : LoadScheduleIntent()
}

data class ScheduleModel(
    val loadingSchedule: Boolean = false,
    val schedule: List<ScheduleItem> = emptyList(),
    val scheduleError: Throwable? = null
)

sealed class SchedulePartialModel {
  object LoadingSchedule : SchedulePartialModel()
  data class ScheduleLoaded(val items: List<ScheduleItem>) : SchedulePartialModel()
  data class ScheduleError(val error: Throwable) : SchedulePartialModel()
}

internal object ScheduleModelReducer : Reducer<ScheduleModel, SchedulePartialModel> {

  override fun invoke(model: ScheduleModel, changes: SchedulePartialModel): ScheduleModel {
    return when (changes) {
      SchedulePartialModel.LoadingSchedule -> ScheduleModel(loadingSchedule = true)
      is SchedulePartialModel.ScheduleLoaded -> ScheduleModel(schedule = changes.items)
      is SchedulePartialModel.ScheduleError -> ScheduleModel(scheduleError = changes.error)
    }
  }
}

data class ScheduleItem(
    val season: String,
    val round: String,
    val time: OffsetDateTime,
    val raceName: String,
    val raceUrl: String,
    val circuitName: String,
    val circuitUrl: String,
    val circuitCountry: String,
    val circuitLocality: String,
    val circuitLat: String,
    val circuitLon: String
) : Comparable<ScheduleItem> {

  override fun compareTo(other: ScheduleItem): Int {
    return this.time.compareTo(other.time)
  }
}

private val ScheduleRecord.scheduleItem: ScheduleItem
  inline get() {
    return ScheduleItem(
        season = season,
        round = round,
        time = time,
        raceName = raceName,
        raceUrl = raceUrl,
        circuitName = circuitName,
        circuitUrl = circuitUrl,
        circuitCountry = circuitCountry,
        circuitLocality = circuitLocality,
        circuitLat = circuitLat,
        circuitLon = circuitLon
    )
  }

class SchedulePresenter @Inject constructor(
    val loadScheduleUseCase: UseCase<LoadScheduleAction, Result<ScheduleRecords>>
) : Presenter<ScheduleView> {

  private val disposables by lazy { CompositeDisposable() }

  override fun attach(view: ScheduleView) {
    val loadSchedule = view.loadScheduleIntent()
        .observeOn(Schedulers.computation())
        .map {
          when (it) {
            is LoadScheduleIntent.CurrentSeason -> LoadScheduleAction.CurrentSeason
            is LoadScheduleIntent.Season -> LoadScheduleAction.Season(it.season)
          }
        }
        .compose(loadScheduleUseCase)
        .map {
          when (it) {
            is Result.Success -> SchedulePartialModel
                .ScheduleLoaded(it.value.map { it.scheduleItem })
            is Result.Failure -> SchedulePartialModel.ScheduleError(it.error)
          }
        }
        .startWith(SchedulePartialModel.LoadingSchedule)
    disposables += loadSchedule.observeOn(Schedulers.computation())
        .scan(ScheduleModel(), ScheduleModelReducer)
        .distinctUntilChanged()
        .subscribeBy(onNext = { view.render(it) })
  }

  override fun detach(view: ScheduleView) {
    disposables.clear()
  }
}
