package io.github.drymarev.neptune.constructor_standings

import com.google.common.truth.Truth.assertThat
import io.github.drymarev.neptune.core.Reducer
import io.github.drymarev.neptune.util.randomUUID
import org.junit.Before
import org.junit.Test
import kotlin.properties.Delegates
import io.github.drymarev.neptune.constructor_standings.ConstructorStandingsModel as Model
import io.github.drymarev.neptune.constructor_standings.ConstructorStandingsPartialModel as PartialModel

class ConstructorStandingsReducerTest {

  private var reducer by Delegates.notNull<Reducer<Model, PartialModel>>()

  @Before fun setUp() {
    reducer = ConstructorStandingsReducer
  }

  @Test fun loadingConstructorStandingsPartialModelProducesCorrectModel() {
    val model = Model(
        loadingConstructorStandings = false,
        constructorStandings = list,
        constructorStandingsError = Throwable()
    )
    val changes = PartialModel.LoadingConstructorStandings
    assertThat(reducer(model, changes)).isEqualTo(model.copy(
        loadingConstructorStandings = true,
        constructorStandings = emptyList(),
        constructorStandingsError = null
    ))
  }

  @Test fun constructorStandingsErrorPartialModelProducesCorrectModel() {
    val model = Model(
        loadingConstructorStandings = true,
        constructorStandings = list,
        constructorStandingsError = null
    )
    val changes = PartialModel.ConstructorStandingsError(Throwable())
    assertThat(reducer(model, changes)).isEqualTo(model.copy(
        loadingConstructorStandings = false,
        constructorStandings = emptyList(),
        constructorStandingsError = changes.error
    ))
  }

  @Test fun constructorStandingsLoadedPartialModelProducesCorrectModel() {
    val model = Model(
        loadingConstructorStandings = true,
        constructorStandings = emptyList(),
        constructorStandingsError = Throwable()
    )
    val changes = PartialModel.ConstructorStandingsLoaded(list)
    assertThat(reducer(model, changes)).isEqualTo(model.copy(
        loadingConstructorStandings = false,
        constructorStandings = changes.items,
        constructorStandingsError = null
    ))
  }
}

private inline val list: List<ConstructorStandingItem> get() = listOf(
    ConstructorStandingItem(
        season = randomUUID(),
        round = randomUUID(),
        position = randomUUID(),
        positionText = randomUUID(),
        points = randomUUID(),
        wins = randomUUID(),
        constructorItem = ConstructorItem(
            randomUUID(),
            randomUUID(),
            randomUUID(),
            randomUUID()
        )
    )
)
