package io.github.drymarev.neptune.schedule

import org.threeten.bp.OffsetDateTime

sealed class LoadScheduleAction {
  object CurrentSeason : LoadScheduleAction()
  data class Season(val season: String) : LoadScheduleAction()
}

data class ScheduleRecord(
    val season: String,
    val round: String,
    val time: OffsetDateTime,
    val raceName: String,
    val raceUrl: String,
    val circuitId: String,
    val circuitName: String,
    val circuitUrl: String,
    val circuitCountry: String,
    val circuitLocality: String,
    val circuitLat: String,
    val circuitLon: String
) : Comparable<ScheduleRecord> {

  override fun compareTo(other: ScheduleRecord): Int {
    return this.time.compareTo(other.time)
  }
}

typealias ScheduleRecords = List<ScheduleRecord>
