package io.github.drymarev.neptune.core

import io.reactivex.ObservableTransformer

typealias UseCase<T, R> = ObservableTransformer<T, R>

sealed class Result<out T> {
  data class Success<out T>(val value: T) : Result<T>()
  data class Failure(val error: Throwable) : Result<Nothing>()
}
